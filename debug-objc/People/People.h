//
//  People.h
//  debug-objc
//
//  Created by DFG on 2019/2/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface People : NSObject

@property (nonatomic) int age;

@end

NS_ASSUME_NONNULL_END
