//
//  main.m
//  debug-objc
//
//  Created by Closure on 2018/12/4.
//

#import <Foundation/Foundation.h>
#import "People/People.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        People *p = [[People alloc] init];
        p.age = 28;
        
        NSLog(@"Hello, World! %d", p.age);
    }
    return 0;
}
